<?php

declare(strict_types=1);

namespace Chriscutts\DiceManagerTests;

use Chriscutts\DiceManager\DiceManager;
use PHPUnit\Framework\TestCase;

class DiceManagerTest extends TestCase
{
    private DiceManager $diceManager;

    public function setUp(): void
    {
        $this->diceManager = new DiceManager();
    }

    public function testInstanceOfDiceManager(): void
    {
        $this->assertInstanceOf(
            DiceManager::class,
            $this->diceManager
        );
    }
}