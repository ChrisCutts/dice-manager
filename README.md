# Dice Manager

A composer project to provide dice management

## Description
A small package to be used for building a collection of dice, rolling them and then getting the results.

## Installation
```
composer require chriscutts/dice-manager
```

## Usage
Examples:
### Create a manager
When creating the manager you can specify the quantity of dice and the number of sides in the first argument, or provide them seperately.
```php
$manager = new DiceManager('2D6');

$manager = new DiceManager(2, 6);
```
### Roll the dice
```php
$manager->roll();
```
### Get the combined total of the rolled dice
```php
$manager->getTotal();
```
### Get the rolled dice
```php
$dice = $manager->getDice();
```
### Loop through the dice and get the results
```php
foreach ($dice as $die) {
    echo $die->getResult();
}
```

